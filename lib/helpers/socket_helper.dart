import 'package:socket_io_client/socket_io_client.dart' as IO;

class SocketIOWrapper {
  final String domain;
  final String user;
  IO.Socket _socket;

  SocketIOWrapper({this.domain, this.user}) {
    _socket = IO.io(domain, <String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': false,
      'extraHeaders': {'user': user}
    });
  }

  connect(String userId) {
    _socket.io.options['extraHeaders'] = {'user': userId};
    _socket.disconnect()..connect();
  }

  subscribeToEvent(String event, Function callback) {
    _socket.on(event, callback);
  }

  unsubscribeToEvent(String event, Function callback) {
    _socket.off(event, callback);
  }

  createCall({String contact, String room, bool isVideo = false, String name}) {
    _socket.emit('CREATE_CALL', {
      'contact': contact,
      'room': room,
      'isVideo': isVideo,
      'lastname': name
    });
  }

  endCall(String room) {
    _socket.emit('END_CALL', {'room': room});
  }

  close() {
    _socket.close();
    _socket = null;
  }
}
