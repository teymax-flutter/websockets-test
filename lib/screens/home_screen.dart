import 'package:flutter/material.dart';
import '../helpers/socket_helper.dart';
import 'user_details_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  SocketIOWrapper _wrapper;
  List<Map<String, dynamic>> _messages = [];

  void addMessage(message) {
    if (_messages.length == 100) {
      _messages.removeLast();
    }
    _messages.insert(0, message);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Messages - ' + _messages.length.toString()),
        actions: [
          IconButton(
            icon: const Icon(Icons.play_circle_fill),
            onPressed: connectAndListen,
          ),
          IconButton(
            icon: const Icon(Icons.stop),
            onPressed: disconnect,
          ),
        ],
      ),
      body: ListView.builder(
        itemCount: _messages.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => UserDetailsScreen(_messages[index]),
                ),
              );
            },
            child: Container(
              decoration: BoxDecoration(
                color: Colors.lightBlueAccent,
                borderRadius: BorderRadius.all(
                  Radius.circular(10),
                ),
              ),
              padding: const EdgeInsets.all(15),
              margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Center(
                child: Text(
                  'Value: ' +
                      _messages[index]['value'] +
                      '\n' +
                      'MessageId: ' +
                      _messages[index]['messageId'] +
                      '\n' +
                      'Type: ' +
                      _messages[index]['type'] +
                      '\n' +
                      'ReceivedAt: ' +
                      _messages[index]['receivedAt'],
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  void connectAndListen() {
    if (_wrapper == null) {
      _wrapper =
          SocketIOWrapper(domain: 'http://192.168.0.103:3000', user: 'artem');
      _wrapper.connect('artem');
      _wrapper.subscribeToEvent('message', updateCallback);
    }
  }

  void disconnect() {
    if (_wrapper != null) {
      _wrapper.unsubscribeToEvent('message', updateCallback);
      _wrapper.close();
      _wrapper = null;
    }
  }

  void updateCallback(message) {
    setState(() {
      addMessage(message);
    });
    print(message);
  }

  @override
  void dispose() {
    super.dispose();
    if (_wrapper != null) {
      _wrapper.close();
      _wrapper = null;
    }
  }
}
