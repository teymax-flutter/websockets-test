import 'package:flutter/material.dart';

class UserDetailsScreen extends StatelessWidget {
  final Map<String, dynamic> message;

  UserDetailsScreen(this.message);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Message'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ListTile(
              title: Text(message['messageId']),
              subtitle: Text('Message Id'),
            ),
            ListTile(
              title: Text(message['type']),
              subtitle: Text('Type'),
            ),
            ListTile(
              title: Text(message['value']),
              subtitle: Text('Value'),
            ),
            ListTile(
              title: Text(message['receivedAt']),
              subtitle: Text('Received at'),
            ),

            // Text('Message Id: ' + message['messageId']),
            // Text('Type: ' + message['type']),
            // Text('Value: ' + message['value']),
            // Text('ReceivedAt: ' + message['receivedAt']),
          ],
        ),
      ),
    );
  }
}
